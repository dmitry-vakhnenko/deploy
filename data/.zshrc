# auto install

if [[ ! -d ~/.zgen ]];then
    git clone --recursive https://github.com/rslindee/zgen.git .zgen
fi

ZGEN_RESET_ON_CHANGE=(~/.zshrc)

eval $(dircolors /etc/DIR_COLORS.lightbgcolor)


# load zgen
source "${HOME}/.zgen/zgen.zsh"


# if the init script doesn't exist
if ! zgen saved; then
  zgen prezto '*:*' case-sensitive 'yes'
  zgen prezto '*:*' color 'yes'
  zgen prezto prompt theme 'peepcode'

  zgen prezto
  zgen prezto history
  zgen prezto utility
  zgen prezto python
  zgen prezto git
  zgen prezto completion
  zgen prezto syntax-highlighting
  zgen load tarruda/zsh-autosuggestions
  zgen prezto history-substring-search
  zgen prezto prompt

  zgen save
fi

alias usevenv="source ~/.venv/bin/activate"
usevenv

export NVS_HOME="$HOME/.nvs"
[ -s "$NVS_HOME/nvs.sh" ] && . "$NVS_HOME/nvs.sh"
