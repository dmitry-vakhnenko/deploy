#!/usr/bin/bash

set -exu

dnf config-manager --set-enabled updates-testing
dnf config-manager --setopt 'fastestmirror=1' --save

dnf install NetworkManager-wifi iwl4965-firmware

dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm \
            https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

dnf install http://mirror.yandex.ru/fedora/russianfedora/russianfedora/free/fedora/russianfedora-free-release-stable.noarch.rpm \
            http://mirror.yandex.ru/fedora/russianfedora/russianfedora/nonfree/fedora/russianfedora-nonfree-release-stable.noarch.rpm \
            http://mirror.yandex.ru/fedora/russianfedora/russianfedora/fixes/fedora/russianfedora-fixes-release-stable.noarch.rpm

rpm --import https://packages.microsoft.com/keys/microsoft.asc
echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo

dnf clean all
dnf update

dnf install glx-utils mesa-dri-drivers plymouth-system-theme xorg-x11-drv-libinput xorg-x11-server-Xorg xorg-x11-utils \
            xorg-x11-xauth xorg-x11-xinit xdg-user-dirs xdg-utils

dnf copr enable heliocastro/hack-fonts
dnf install hack-fonts

dnf install i3 i3lock sddm sddm-themes alsa-plugins-pulseaudio xclip

dnf install zsh fpaste nano git tree htop feh sqlite tar

dnf install telegram-desktop chromium code

# systemctl set-default graphical
# systemctl enable sddm pulseaudio